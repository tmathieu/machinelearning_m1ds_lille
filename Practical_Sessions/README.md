
If python is not set up on your computer:

You can use this link:

https://tmathieu.gitlabpages.inria.fr/machinelearning_m1ds_lille/lab/index.html

which should open a jupyter page **in the web browser** from which the practical session can be executed. If you use this link, do not forget to download the resulting notebook (with file > download) at the end because the editted version of the notebook is only in the browser cache.
This is experimental at best, I did not test it in depth but this seems to work for now.



