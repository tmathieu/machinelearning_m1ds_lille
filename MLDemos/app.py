import time
import importlib

import dash
import dash_core_components as dcc
import dash_html_components as html
import numpy as np
from dash.dependencies import Input, Output, State
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn import datasets
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline

import utils.dash_reusable_components as drc
import utils.figures as figs

app = dash.Dash(
    __name__,
    meta_tags=[
        {"name": "viewport", "content": "width=device-width, initial-scale=1.0"}
    ],
)
app.title = "Machine Learning"
server = app.server


def generate_data(n_samples, dataset, noise, random_state):
    if dataset == "moons":
        return datasets.make_moons(n_samples=n_samples, noise=noise, random_state=random_state)

    elif dataset == "circles":
        return datasets.make_circles(
            n_samples=n_samples, noise=noise, factor=0.5, random_state=random_state
        )
    elif dataset == "blobs":
        return datasets.make_blobs(
            n_samples=n_samples, centers=[[-2,0], [2,0]], random_state=random_state, cluster_std=1 + noise
        )

    elif dataset == "linear":
        X, y = datasets.make_classification(
            n_samples=n_samples,
            n_features=2,
            n_redundant=0,
            n_informative=2,
            random_state=random_state,
            n_clusters_per_class=1,
        )

        rng = random_state
        X += noise * rng.uniform(size=X.shape)
        linearly_separable = (X, y)

        return linearly_separable

    else:
        raise ValueError(
            "Data type incorrectly specified. Please choose an existing dataset."
        )


app.layout = html.Div(
    children=[
        # .container class is fixed, .container.scalable is scalable
        html.Div(
            className="banner",
            children=[
                # Change App Name here
                html.Div(
                    className="container scalable",
                    children=[
                        # Change App Name here
                        html.H2(
                            id="banner-title",
                            children=[
                                html.A(
                                    "Machine Learning Explorer",
                                    style={
                                        "text-decoration": "none",
                                        "color": "inherit",
                                    },
                                )
                            ],
                        ),
                        
                    ],
                )
            ],
        ),
        html.Div(
            id="body",
            className="container scalable",
            children=[
                html.Div(
                    id="app-container",
                    # className="row",
                    children=[
                        html.Div(
                            # className="three columns",
                            id="left-column",
                            children=[
                                drc.Card(
                                    id="first-card",
                                    children=[
                                        drc.NamedDropdown(
                                            name="Select Algorithm",
                                            id="dropdown-select-algo",
                                            options=[
                                                {
                                                    "label": "Logistic regression",
                                                    "value": "linear-algo",
                                                },
                                                {
                                                    "label": "Nearest Neighbor",
                                                    "value": "nn-algo",
                                                },
                                                    {
                                                    "label": "SVM",
                                                    "value": "svm-algo",
                                                },
                                                    {
                                                    "label": "Neural Network",
                                                    "value": "mlp-algo",
                                                },
                                            ],
                                            clearable=False,
                                            searchable=False,
                                            value="linear-algo",
                                        ),
                                        drc.NamedDropdown(
                                            name="Select Dataset",
                                            id="dropdown-select-dataset",
                                            options=[
                                                {"label": "Blobs", "value": "blobs"},
                                                {
                                                    "label": "Random classification",
                                                    "value": "linear",
                                                },
                                                {"label": "Moons", "value": "moons"},
                                                {
                                                    "label": "Circles",
                                                    "value": "circles",
                                                },
                                            ],
                                            clearable=False,
                                            searchable=False,
                                            value="blobs",
                                        ),
                                        drc.NamedSlider(
                                            name="Polynomial basis",
                                            id="slider-poly-level",
                                            min=1,
                                            max=5,
                                            value=1,
                                            step=1,
                                        ),
                                        drc.NamedSlider(
                                            name="Train Sample Size",
                                            id="slider-dataset-sample-size",
                                            min=10,
                                            max=500,
                                            step=10,
                                            marks={
                                                str(i): str(i)
                                                for i in [10,100, 200, 300, 400, 500]
                                            },
                                            value=300,
                                        ),
                                        drc.NamedSlider(
                                            name="Noise Level",
                                            id="slider-dataset-noise-level",
                                            min=0,
                                            max=1,
                                            marks={
                                                i / 10: str(i / 10)
                                                for i in range(0, 11, 2)
                                            },
                                            step=0.1,
                                            value=0.2,
                                        ),
                                        drc.NamedSlider(
                                            name="Data Seed",
                                            id="slider-seed",
                                            min=1,
                                            max=300,
                                            value=42,
                                            marks={
                                                i : str(i)
                                                for i in range(0,300,100)
                                            },
                                            step=1,
                                        )
                                    ],
                                ),
                                drc.Card(
                                    id="last-card",
                                    children=[
                                        html.Div(
                                            id="shrinking-container",
                                            children=[
                                                
                                                
                                                drc.Card(
                                                    id="button-card",
                                                    children=[
                                                        drc.NamedSlider(
                                                            name="Nb neighbors",
                                                            id="slider-nbneighbors-level",
                                                            min=1,
                                                            max=5,
                                                            value=1,
                                                            step=1,
                                                        ),
                                                    ],
                                                ),
                                                drc.NamedDropdown(
                                                    name="Kernel",
                                                    id="dropdown-svm-parameter-kernel",
                                                    options=[
                                                        {
                                                            "label": "Radial basis function (RBF)",
                                                            "value": "rbf",
                                                        },
                                                        {"label": "Linear", "value": "linear"},
                                                        {
                                                            "label": "Polynomial",
                                                            "value": "poly",
                                                        },
                                                        {
                                                            "label": "Sigmoid",
                                                            "value": "sigmoid",
                                                        },
                                                    ],
                                                    value="rbf",
                                                    clearable=False,
                                                    searchable=False,
                                                ),
                                                 drc.Card(
                                                    id="button-card2",
                                                    children=[
                                                        drc.NamedSlider(
                                                            name="Nb neurons",
                                                            id="slider-neurons-level",
                                                            min=1,
                                                            max=300,
                                                            value=50,
                                                            marks={
                                                                i : str(i)
                                                                for i in range(0,300,100)
                                                            },
                                                            step=10,
                                                        ),
                                                    ],
                                                )
                                            ],
                                        ),
                                    ],
                                ),
                            ],
                        ),
                        html.Div(
                            id="div-graphs",
                            children=dcc.Graph(
                                id="graph-sklearn",
                                figure=dict(
                                    layout=dict(
                                        plot_bgcolor="#282b38", paper_bgcolor="#282b38"
                                    )
                                ),
                            ),
                        ),
                    ],
                )
            ],
        ),
    ]
)



@app.callback(
    Output("slider-nbneighbors-level", "disabled"),
    [Input("dropdown-select-algo", "value")],
)
def disable_slider_neighbors(algo):
    return algo not in ["nn-algo"]


@app.callback(
    Output("dropdown-svm-parameter-kernel", "disabled"),
    [Input("dropdown-select-algo", "value")],
)
def disable_slider_svm(algo):
    return algo not in ["svm-algo"]

@app.callback(
    Output("slider-neurons-level", "disabled"),
    [Input("dropdown-select-algo", "value")],
)
def disable_slider_sml(algo):
    return algo not in ["mlp-algo"]

@app.callback(
    Output("div-graphs", "children"),
    [
        Input("dropdown-select-algo", "value"),
        Input("dropdown-svm-parameter-kernel", "value"),
        Input("slider-nbneighbors-level", "value"),
        Input("dropdown-select-dataset", "value"),
        Input("slider-dataset-noise-level", "value"),
        Input("slider-dataset-sample-size", "value"),
        Input("slider-neurons-level", "value"),
        Input("slider-seed", "value"),
        Input("slider-poly-level", "value")
    ],
)



def update_svm_graph(
    algo,
    kernel,
    neighbors,
    dataset,
    noise,
    sample_size,
    nb_neurons,
    seed,
    poly_degree
):
    def reverse_cols(X):
        return X[:,list(np.arange(1,len(X[0])))+[0]]
    t_start = time.time()
    h = 0.05  # step size in the mesh

    # Data Pre-processing
    X, y = generate_data(n_samples=sample_size+200, dataset=dataset, noise=noise, random_state = np.random.RandomState(seed))
    Xplot = X.copy()
    
    
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=200, random_state=42
    )
    Xplot_train = X_train.copy()
    Xplot_test = X_test.copy()

    transformer = PolynomialFeatures(degree=poly_degree,interaction_only= False)
    
    if poly_degree > 1:
        X_train = transformer.fit_transform(X_train)
        print(X_train.shape)
        X_train = reverse_cols(X_train)
        print(X_train.shape)
        X_test = transformer.transform(X_test)
        X_test = reverse_cols(X_test)
        
    # scaler = StandardScaler()
    # X_train = scaler.fit_transform(X_train)
    # X_test  = scaler.transform(X_test)
        

    x_min = Xplot[:, 0].min() - 0.5
    x_max = Xplot[:, 0].max() + 0.5
    y_min = Xplot[:, 1].min() - 0.5
    y_max = Xplot[:, 1].max() + 0.5
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    
    # Train algo
    if algo == "svm-algo":
        clf = SVC( kernel=kernel)
    elif algo == "linear-algo":
        clf = LogisticRegression(penalty=None)
    elif algo == "mlp-algo":
        clf = MLPClassifier((nb_neurons//2,nb_neurons//2))
    elif algo == "nn-algo":
        clf = KNeighborsClassifier(n_neighbors=neighbors)

    clf.fit(X_train, y_train)

    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, x_max]x[y_min, y_max].

    xplot = np.c_[xx.ravel(), yy.ravel()]
    if poly_degree > 1:
        xplot = reverse_cols(transformer.transform(xplot))

    if hasattr(clf, "decision_function"):
        Z = clf.decision_function(xplot)
        threshold = 0.5
    else:
        Z = clf.predict_proba(xplot)[:, 1]
        threshold = 0.5
        
    prediction_figure = figs.serve_prediction_plot(
        model=clf,
        X_train=X_train,
        X_test=X_test,
        y_train=y_train,
        y_test=y_test,
        Z=Z,
        xx=xx,
        yy=yy,
        mesh_step=h,
        threshold=threshold,
        Xplot_train=Xplot_train,
        Xplot_test=Xplot_test
    )

    roc_figure = figs.serve_roc_curve(model=clf, X_test=X_test, y_test=y_test)

    confusion_figure = figs.serve_pie_confusion_matrix(
        model=clf, X_test=X_test, y_test=y_test, Z=Z, threshold=0.5
    )

    return [
        html.Div(
            id="svm-graph-container",
            children=dcc.Loading(
                className="graph-wrapper",
                children=dcc.Graph(id="graph-sklearn", figure=prediction_figure),
                style={"display": "none"},
            ),
        ),
        html.Div(
            id="graphs-container",
            children=[
                dcc.Loading(
                    className="graph-wrapper",
                    children=dcc.Graph(id="graph-line-roc-curve", figure=roc_figure),
                ),
                dcc.Loading(
                    className="graph-wrapper",
                    children=dcc.Graph(
                        id="graph-pie-confusion-matrix", figure=confusion_figure
                    ),
                ),
            ],
        ),
    ]


# Running the server
if __name__ == "__main__":
    app.run_server(debug=True)
