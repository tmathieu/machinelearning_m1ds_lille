# Machine Learning Explorer

This folder contains an interactive demo of basic Machine Learning Classification algorithms.

## Running the app locally

First create a virtual environment with conda or venv inside a temp folder, then activate it.

```
virtualenv mldemos_venv

# Windows
mldemos_venv\Scripts\activate
# Or Linux
source mldemos_venv/bin/activate
```

Clone the git repo, then install the requirements with pip
```
git clone https://gitlab.inria.fr/tmathieu/machinelearning_m1ds_lille
cd machinelearning_m1ds_lille/MLDemos
pip install -r requirements.txt
```

Run the app
```
python app.py
```
## Acknowledgments
This folder is basically a rewriting of  [plotly dash-svm](https://github.com/plotly/dash-sample-apps/tree/main/apps/dash-svm). 

