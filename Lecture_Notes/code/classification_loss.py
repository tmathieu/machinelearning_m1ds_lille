import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-3,3)
fig, ax = plt.subplots(figsize=(6,4))
ax.plot(x, np.exp(x), label="$\exp(x)$")
ax.plot(x, (1+x)*(x>-1), label="$(1+x)_+$")
ax.plot(x, np.log(1+np.exp(x))/np.log(2), label="$\log_2(1+e^x)$")
plt.legend()
plt.ylim(-0.5,3.5)


ax.set_yticks([ 1, 2,3])
ax.set_xticks([-3,-2,-1, 1, 2,3])


ax.spines["bottom"].set_position(("data", 0))
ax.spines["left"].set_position(("data", 0))
ax.spines.right.set_visible(False)
ax.spines.top.set_visible(False)

fig.savefig("FIG/cost_functions.pdf")
