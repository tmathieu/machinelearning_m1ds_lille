import numpy as np
from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.inspection import DecisionBoundaryDisplay

X, y = make_classification(
    n_features=2, n_redundant=0, n_informative=2, random_state=1, n_clusters_per_class=1, n_samples=50
)
rng = np.random.RandomState(3)
X += rng.uniform(size=X.shape)*4

cmap_light = ListedColormap(["orange", "cyan", "cornflowerblue"])
cmap_bold = ["darkorange", "c", "darkblue"]

def plot_classif(clf, X, y, ax):
    DecisionBoundaryDisplay.from_estimator(
        clf,
        X,
        cmap=plt.cm.Paired,
        ax=ax,
        response_method="predict",
        shading="auto",
        grid_resolution=200
       )
    ax.scatter(X[:, 0], X[:, 1], c=y)


clf1 = LogisticRegression()
clf2 = KNeighborsClassifier(n_neighbors=2)

clf1.fit(X,y)
clf2.fit(X,y)
    
fig, axs = plt.subplots(1,2,figsize=(10,3))

plot_classif(clf1, X, y, axs[0])
plot_classif(clf2, X, y, axs[1])
fig.savefig("FIG/overfitting.pdf")
