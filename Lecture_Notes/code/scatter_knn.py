import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from sklearn.neighbors import KNeighborsRegressor, KNeighborsClassifier
from sklearn.inspection import DecisionBoundaryDisplay
from matplotlib.colors import ListedColormap

fig, ax = plt.subplots(figsize=(5,3))
cmap_light = ListedColormap(["orange", "cyan", "cornflowerblue"])
cmap_bold = ["darkorange", "c", "darkblue"]

def plot_classif(clf, X, y, ax):
    DecisionBoundaryDisplay.from_estimator(
        clf,
        X,
        cmap=plt.cm.Paired,
        ax=ax,
        response_method="predict",
        shading="auto",
        grid_resolution=200
       )
    ax.scatter(X[:, 0], X[:, 1], c=y)
X, y = make_blobs(centers=[[-3,0],[3,0]])
clf = KNeighborsClassifier(n_neighbors=3)
clf.fit(X,y)
plot_classif(clf, X, y, ax)

fig.savefig("FIG/classification_nn_point_cloud.pdf")
plt.show()


fig, ax = plt.subplots(figsize=(5,3))
X = np.random.uniform(size=100)*3
y = 3*X + np.random.normal(size=len(X))
reg = KNeighborsRegressor(n_neighbors=3)
reg.fit(X[:,np.newaxis], y)
ax.scatter(X,  y)
xplot = np.linspace(X.min(), X.max())
ax.plot(xplot, reg.predict(xplot[:,np.newaxis]), color="black")
fig.savefig("FIG/regression_nn_point_cloud.pdf")
plt.show()
