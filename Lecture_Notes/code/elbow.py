import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures

def true_fun(X):
    return np.cos(1.5 * np.pi * X)

np.random.seed(0)

n_samples = 30
degrees = np.arange(0,10)
scores = []
scores_t = []
plt.subplots(figsize=(5,3))

for i in range(len(degrees)):
    scores_degree = []
    scores_t_degree = []
    for _ in tqdm(range(1000)): # Do 1000 Monte-Carlo approximation of error
        X = np.sort(np.random.rand(n_samples))
        y = true_fun(X) + np.random.randn(n_samples) * 0.1
        
        Xtest = np.sort(np.random.rand(4*n_samples))
        ytest = true_fun(Xtest) + np.random.randn(4*n_samples) * 0.1
        polynomial_features = PolynomialFeatures(degree=degrees[i])
        linear_regression = LinearRegression()
        pipeline = Pipeline(
            [
                ("polynomial_features", polynomial_features),
                ("linear_regression", linear_regression),
            ]
        )
        pipeline.fit(X[:, np.newaxis], y)

        # Evaluate the models using crossvalidation
        scores_degree.append(np.mean((pipeline.predict(Xtest[:, np.newaxis]) - ytest)**2))
        scores_t_degree.append(np.mean((pipeline.predict(X[:, np.newaxis]) - y)**2))
    scores.append(np.mean(scores_degree))
    scores_t.append(np.mean(scores_t_degree))
plt.plot(degrees, scores, label="Generalization error")
plt.plot(degrees, scores_t, label="Training error")
plt.ylabel("MSE")
plt.xlabel("degree polynomial")
plt.ylim((0,0.8))
plt.tight_layout()
plt.legend()
plt.savefig('FIG/elbow_curve.pdf')
plt.show()
