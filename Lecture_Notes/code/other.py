import numpy as np
import matplotlib.pyplot as plt
p = np.linspace(0,1)
n = 10
plt.plot(p, (1-p)**(n+1)+p**(n+1))
plt.show()
