import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_moons
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.inspection import DecisionBoundaryDisplay
from sklearn.preprocessing import PolynomialFeatures
from matplotlib.colors import ListedColormap

fig, ax = plt.subplots(figsize=(5,3))
cmap_light = ListedColormap(["orange", "cyan", "cornflowerblue"])
cmap_bold = ["darkorange", "c", "darkblue"]
transformer = PolynomialFeatures(degree=3)


def plot_classif(clf, X, y, ax, transformer):
    # plotting this decision boundary is more tricky because we want to plot only the
    # two first coordinates of X
    feature_1, feature_2 = np.meshgrid(
        np.linspace(X[:, 0].min(), X[:, 0].max()),
        np.linspace(X[:, 1].min(), X[:, 1].max())
    )
    xplot = np.c_[feature_1.ravel(), feature_2.ravel()]
    xplot = transformer.transform(xplot)
    xplot = xplot[:,list(np.arange(1,len(xplot[0])))+[0]]
    
    display = DecisionBoundaryDisplay(xx0=feature_1, xx1=feature_2,
                                      response=clf.predict(xplot).reshape(feature_1.shape))
    display.plot(ax=ax,cmap=plt.cm.Paired)
    ax.scatter(X[:, 0], X[:, 1], c=y)
    
X, y = make_moons(noise=0.1)
clf = LogisticRegression()
X = transformer.fit_transform(X)
X = X[:,list(np.arange(1,len(X[0])))+[0]] # put the first column corresponding to intercept (all 1) to the end for plot.
clf.fit(X,y)
plot_classif(clf, X, y, ax, transformer)

fig.savefig("FIG/classification_poly_logreg_point_cloud.pdf")
plt.show()

fig, ax = plt.subplots(figsize=(5,3))
transformer = PolynomialFeatures(degree=3)
X = np.random.uniform(size=100)*3
y = 3*np.sin(X) + np.random.normal(size=len(X))
xplot = np.linspace(X.min(), X.max())
reg = LinearRegression()
X_preproc = transformer.fit_transform(X[:,np.newaxis])
X_preproc = X_preproc[:,list(np.arange(1,len(X_preproc[0])))+[0]] # put the first column corresponding to intercept (all 1) to the end for plot.
reg.fit(X_preproc, y)
ax.scatter(X,  y)
xplot_preproc = transformer.transform(xplot[:,np.newaxis])
xplot_preproc = xplot_preproc[:,list(np.arange(1,len(xplot_preproc[0])))+[0]]
ax.plot(xplot, reg.predict(xplot_preproc), color="black")
fig.savefig("FIG/regression_poly_linreg_point_cloud.pdf")
plt.show()
