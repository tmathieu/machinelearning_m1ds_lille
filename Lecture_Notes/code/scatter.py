import numpy as np

import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
fig, ax = plt.subplots(figsize=(5,3))
X, y = make_blobs(centers=[[-3,0],[3,0]])
ax.scatter(X[:,0],X[:,1], c=y)
fig.savefig("FIG/classification_point_cloud.pdf")
plt.show()
fig, ax = plt.subplots(figsize=(5,3))

X = np.random.uniform(size=100)*3
y = 3*X + np.random.normal(size=len(X))
ax.scatter(X,  y)
fig.savefig("FIG/regression_point_cloud.pdf")
plt.show()
