import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-3,3)
alpha = 0.3

y_mse = x**2
y_mae = np.abs(x)
y_pin = alpha*x*(x>0) - (1-alpha)*x*(x<=0)
plt.subplots(figsize=(5,3))
plt.plot(x, y_mse, label="Squared loss")
plt.plot(x, y_mae, label="Absolute loss")
plt.plot(x, y_pin, label="Pinball loss")
plt.legend()
plt.savefig("FIG/regression_losses.pdf")
plt.show()
